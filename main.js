const routes = [
	{ path: `${CONFIG.FRONTEND_BASE_URL}/`, component: Homepage },
	{ path: `${CONFIG.FRONTEND_BASE_URL}/stream/:id`, component: StreamDisplay }
]

const router = new VueRouter({
	mode: 'history',
	routes 
})


Loader = Vue.component('loader', {
	props: [],
	template: '#loader',
	methods: {

	},
	data () {
		return {
			shared: store.state
		}
	}
})

Prompt = Vue.component('prompt', {
	props: [],
	template: '#prompt',
	methods: {
		close: function() {
			store.methods.hidePrompt();
		},
		refresh: function() {
			window.location.reload();
		}
	},
	data() {
		return {
			shared: store.state
		}
	}
})

var app = new Vue({
	el: '#app',
	data: {
		shared: store.state
	},
	mounted() {
		store.methods.loadTeamList();
		store.methods.getStadiumList();
		store.methods.populateEmptyTeamData();
	},
	router,
	store: vxstore
})