MatchCreator = Vue.component('match-creator', {
	props: [],
	template: '#match-creator',
	methods: {
		teamChanged: function(teamNumber) {
			if(teamNumber === 1) {
				let index = this.shared.teamList.findIndex((e) => e.id == this.private.team1);
				if (index === -1) {
					this.private.team1 = -1;
					this.shared.matchSettings.team1 = {
						id: undefined,
						name: undefined,
						shortform: undefined,
						playerList: []
					} 
					return
				} 
				this.shared.matchSettings.team1 = {
					id: this.shared.teamList[index].id,
					name: this.shared.teamList[index].name,
					shortform: this.shared.teamList[index].shortform,
					playerList: []
				} 
				store.methods.getTeamDetails(1, this.shared.teamList[index].id)
			} else {
				let index = this.shared.teamList.findIndex((e) => e.id == this.private.team2);
				if (index === -1) {
					this.private.team1 = -1;
					this.shared.matchSettings.team2 = {
						id: undefined,
						name: undefined,
						shortform: undefined,
						playerList: []
					}
					return
				} 
				this.shared.matchSettings.team2 = {
					id: this.shared.teamList[index].id,
					name: this.shared.teamList[index].name,
					shortform: this.shared.teamList[index].shortform,
					playerList: []
				}
				store.methods.getTeamDetails(2, this.shared.teamList[index].id)
			}
		},

		simulateMatch() {
			let data = {
				team1Id: this.shared.matchSettings.team1.id,
				team2Id: this.shared.matchSettings.team2.id,
				stadiumId: this.private.stadiumId
			}
			API.queueMatch(data).then((response) => {
				router.push({ path: `${CONFIG.FRONTEND_BASE_URL}/stream/${response}` })
			})
		}
	},
	computed: {
		hasTeamListLoaded: function () {
			return this.shared.teamList.length != 0
		},
		anyFieldEmpty: function () {
			if(this.private.team1 === this.private.team2) {
				return true;
			}
			return this.private.team1 === -1 || this.private.team2 === -1 || this.private.stadiumId === -1;
		}
	},
	data () {
		return {
			private: {
				team1: -1,
				team2: -1,
				stadiumId: -1
			},
			shared: store.state
		}
	}
})

TeamInfoDisplay = Vue.component('team-info-display', {
	props: [],
	template: '#team-info-display',
	methods: {
		
	},
	data () {
		return {
			shared: store.state
		}
	}
})

TeamInfoGrid = Vue.component('team-info-grid', {
	props:  {
		teamData: Object,
		alignRight: Boolean,
		alignLeft: Boolean
	},
	template: '#team-info-grid',
	methods: {
		log: function() {
			console.log(this.shared.matchSettings.team1.teamData)
		}
	},
	data () {
		return {
			shared: store.state
		}
	},
	mounted() {
		// if(this.teamNumber == 1) {
		// 	this.teamData = this.shared.matchSettings.team1;
		// } else {
		// 	this.teamData = this.shared.matchSettings.team2;
		// }	
	}
})

PlayerCell = Vue.component('player-cell', {
	props: {
		alignLeft: Boolean,
		alignRight: Boolean,
		player: Object
	},
	template: '#player-cell'
})

Homepage = Vue.component('homepage', {
	template: '#homepage',
});