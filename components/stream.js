StreamDisplay = Vue.component('stream-display', {
	props: [],
	template: '#stream-display',
	methods: {
		loadMatchData: function(id) {
			let promise = new Promise((resolve, reject) => {	
				store.methods.setLoading(true);
				API.getMatchData(id).then((data) => {
					if(data.finished === 0 ) {
						store.methods.setLoading(false);
						store.methods.showPrompt(CONSTS.MSG.PROCESSING_MESSAGE, true);
						setTimeout(() => {
							window.location.reload();
						}, 5000);
						resolve(false);
					}
					this.$store.commit(M.SET_MATCH_DATA, data);
					Promise.all([API.getTeamDetails(this.team1Id), 
								API.getTeamDetails(this.team2Id), 
								API.getStadiumList(this.stadiumId)]).then((data) => {
									this.$store.commit(M.SET_TEAM_DATA, data[0]);
									this.$store.commit(M.SET_TEAM_DATA, data[1]);
									this.$store.commit(M.SET_STADIUM_DATA, data[2][0]);
									resolve(true);
								});
				});
			});
			return promise;
		},
		initializeMatchState: function(f) {
			this.$store.commit(M.INIT_MATCH_STATE);
			this.isReady = true;
			if (f === 'y') {
				if(this.matchState !== 'finished') {
					this.$store.commit(M.FF);
					store.methods.setLoading(true);
				}
			} else {
				store.methods.setLoading(false);
			}
		},
		processMatchData () {
			this.processInningData(1).then((data) => {
				console.log("First innings concluded.")
				this.$store.commit(M.CONCLUDE_INNINGS, 1);
				this.processInningData(2).then((data) => {
					this.$store.commit(M.CONCLUDE_INNINGS, 2);
					store.methods.setLoading(false);
					this.$store.commit(M.REVERT_FF);
				});
			});
		},
		processInningData (inning) {
			let anon = (resolve, reject) => {
				this.processBallData();
				if(this.currentInning === 1) {
					if(this.battingTeam.score.currentBall >= this.firstInning.length) {
						// clearInterval(this.intervalId);
						resolve();
					}
				} else if (this.currentInning === 2) {
					if(this.battingTeam.score.currentBall >= this.secondInning.length) {
						// clearInterval(this.intervalId);
						resolve();
						return;
					}
				}
				this.intervalId = setTimeout(() => anon(resolve, reject), this.intervalStep);
				// anon(resolve, reject);
			};

			let promise = new Promise((resolve, reject) => {
				this.intervalId = setTimeout(() => anon(resolve, reject), this.intervalStep);
			})
			return promise;
		},
		processBallData () {
			if(this.matchState === 'paused') {
				return;
			}
			let inningData = null;
			let ballData = null;
			if(this.currentInning === 1) {
				inningData = this.firstInning;
			} else if (this.currentInning === 2){
				inningData = this.secondInning;
			} else {
				console.error("FATAL: state.currentInning value out of bounds.");
			}

			//check for inning conclusion 
			if(this.battingTeam.score.currentBall >= inningData.length) {
				clearInterval(this.intervalId);
				return;
			}

			ballData = inningData[this.battingTeam.score.currentBall];
			let payload = {
				ballData: ballData,
				isOut: this.isOut(ballData),
				isFreehit: this.isFreehit(inningData, ballData)
			}
			this.$store.commit(M.INIT_BALL_PROCESS, ballData);
			if(this.isBallValid(ballData)) {
				//handlevalidball
				this.$store.commit(M.PROCESS_VALID_BALL, payload);
			} else if(ballData.bowling_outcome === CONSTS.BOWLING_OUTCOMES.WIDE) {
				this.$store.commit(M.PROCESS_WIDE_BALL, payload);
			} else if(ballData.bowling_outcome === CONSTS.BOWLING_OUTCOMES.NO_BALL) {
				this.$store.commit(M.PROCESS_NO_BALL, payload);
			} else {
				console.error(`Invalid bowling outcome for ball: ${ballData}`);
			}
			this.$store.commit(M.POST_BALL_PROCESS, payload);
		},
		isBallValid(ballData) {
			if( ballData.bowling_outcome === CONSTS.BOWLING_OUTCOMES.WIDE ||
				ballData.bowling_outcome === CONSTS.BOWLING_OUTCOMES.NO_BALL) {
					return false;
				}
			return true;
		},
		//This doesn't take into account if the ball was a freehit.
		isOut(ballData) {
			if( ballData.batting_outcome === CONSTS.DISMISSAL.CAUGHT ||
				ballData.batting_outcome === CONSTS.DISMISSAL.RUNOUT ||
				ballData.batting_outcome === CONSTS.DISMISSAL.BOWLED ||
				ballData.batting_outcome === CONSTS.DISMISSAL.STUMPED ||
				ballData.batting_outcome === CONSTS.DISMISSAL.LBW ||
				ballData.batting_outcome === CONSTS.DISMISSAL.HIT_WICKET) {
					return true;
				}
			return false;
		},
		isFreehit(inningData, ballData) {
			if(this.battingTeam.score.currentBall == 0) {
				return false;
			} else {
				let prevBallData = inningData[this.battingTeam.score.currentBall - 1];
				if(prevBallData.bowling_outcome === CONSTS.BOWLING_OUTCOMES.NO_BALL) {
					return true;
				}
				return false;
			}
		}
	},
	mounted() {
		this.matchId = this.$route.params.id;
		let f = this.$route.query.f;

		let payload = {
			link: `${CONFIG.BACKEND_BASE_URL}${this.$route.path}?f=y`
		}

		this.$store.commit(M.SET_SC_LINK, payload);

		this.loadMatchData(Number.parseInt(this.matchId)).then((data) => {
			if(data === false) {
				return;
			}
			this.initializeMatchState(f);
			this.processMatchData();
		});
	},
	computed : {
		... Vuex.mapState({
			team1Id: (state) => state.matchData.team1Id,
			team2Id: (state) => state.matchData.team2Id,
			stadiumId: (state) => state.matchData.stadiumId,
			firstInning: (state) => state.matchData.matchData[0].inning_data,
			secondInning: (state) => state.matchData.matchData[1].inning_data,
			currentInning: (state) => state.currentInning,
			team1: (state) => state.team1,
			team2: (state) => state.team2,
			stadium: (state) => state.stadium,
			matchState: (state) => state.matchState,
			intervalStep: (state) => state.intervalStep,
			isFF: (state) => state.isFF
		}),
		... Vuex.mapGetters({
			battingTeam: 'getBattingTeam',
			bowlingTeam: 'getBowlingTeam'
		})
	},
	data () {
		return {
			matchId: 0,
			isReady: false,
			intervalId: null
		}
	}
})

StreamMatchInfo = Vue.component('stream-match-info', {
	props: ["team1", "team2", "stadium"],
	template: '#stream-match-info',
})

StreamScorecardDisplay = Vue.component('stream-scorecard-display', {
	props: [],
	template: '#stream-scorecard-display',
	computed : {
		... Vuex.mapState({
			inningsOrder: (state) => state.inningsOrder,
			currentInning: (state) => state.currentInning
		})
	}
})

StreamScorecard = Vue.component('stream-scorecard', {
	props: {
		battingTeamId: Number,
		bowlingTeamId: Number
	},
	template: '#stream-scorecard',
	computed : {
		... Vuex.mapState({
			currentInning: (state) => state.currentInning,
			team1: (state) => state.team1,
			team2: (state) => state.team2,
			onStrike: (state) => state.onStrike,
			runnerEnd: (state) => state.runnerEnd,
			currentBowler: (state) => state.currentBowler
		}),
		... Vuex.mapGetters({
			battingTeam: 'getBattingTeam',
			bowlingTeam: 'getBowlingTeam',
			getTeamById: 'getTeamById'
		})
	}
})

BatsmanCell = Vue.component('batsman-cell', {
	props: ["batsman"],
	template: '#batsman-cell',
	methods: {
		getSR: function(batsman) {
			if (batsman.batting.runs === null) {
				return ''
			} else {
				if(batsman.batting.balls === 0 || batsman.batting.runs === 0) {
					return '0';
				}
				return ((batsman.batting.runs/batsman.batting.balls) * 100).toFixed(2);
			}
		}
	},
	computed: {
		... Vuex.mapState({
			onStrike: (state) => state.onStrike,
			runnerEnd: (state) => state.runnerEnd,
			currentBowler: (state) => state.currentBowler
		})
	}
});


BowlerCell = Vue.component('bowler-cell', {
	props: ["bowler"],
	template: '#bowler-cell',
	methods: {
		getOvers: function(bowler) {
			if(bowler.bowling.balls === 0 || bowler.bowling.runs === 0) {
				return '0';
			}
			let overs = Math.floor(bowler.bowling.balls/6);
			let balls = bowler.bowling.balls % 6;
			return `${overs}.${balls}`;
		},
		getER: function(bowler) {
			if(bowler.bowling.balls === 0 || bowler.bowling.runs === 0) {
				return '0';
			}
			let overs = Math.floor(bowler.bowling.balls/6);
			return (bowler.bowling.runs/overs).toFixed(2);
		}
	}
});

Commentary = Vue.component('commentary', {
	props: ["data"],
	template: '#commentary',
	methods: {
		getClass: function(line) {
			if (line.outcome === 'boundary') {
				return 'ball-data-boundary';
			} else if (line.outcome === 'wicket') {
				return 'ball-data-wicket';
			} else if (line.outcome === 'wide' || line.outcome === 'noball') {
				return 'ball-data-extra';
			} else {
				return ''
			}
		}
	},
	computed: {
		... Vuex.mapState({
			commentary: (state) => {
				return JSON.parse(JSON.stringify(state.commentary)).reverse();
			}
		})
	}
})

ScoreboardDisplay = Vue.component('scoreboard-display', {
	props: [],
	template: '#scoreboard-display',
	methods: {
		getBallDisplayText: function(ball) {
			let text = '';
			if(ball.ballOc === CONSTS.BOWLING_OUTCOMES.WIDE) {
				return 'Wi';
			} else if (ball.ballOc === CONSTS.BOWLING_OUTCOMES.NO_BALL) {
				return 'Nb';
			}
			if( ball.batOc === CONSTS.DISMISSAL.CAUGHT ||
				ball.batOc === CONSTS.DISMISSAL.RUNOUT ||
				ball.batOc === CONSTS.DISMISSAL.BOWLED ||
				ball.batOc === CONSTS.DISMISSAL.STUMPED ||
				ball.batOc === CONSTS.DISMISSAL.LBW) {
					return 'W';
				}
			// if(ball.batOc === '4' || ball.batOc === '6' || ball.batOc === 6) {
			// 	console.log(`${ball.batOc} ${typeof ball.batOc}`);
			// }
			return ball.batOc;
		}
	},
	computed : {
		... Vuex.mapState({
			currentInning: (state) => state.currentInning,
			team1: (state) => state.team1,
			team2: (state) => state.team2,
			onStrike: (state) => state.onStrike,
			runnerEnd: (state) => state.runnerEnd,
			currentBowler: (state) => state.currentBowler
		}),
		... Vuex.mapGetters({
			battingTeam: 'getBattingTeam',
			bowlingTeam: 'getBowlingTeam'
		})
	},
})

StreamStatusControl = Vue.component('match-status-control', {
	props: [],
	template: '#match-status-control',
	methods: {
		toggleMatchState: function() {
			this.$store.commit(M.TOGGLE_MATCH_STATE);
		},
		speedUp: function() {
			this.$store.commit(M.DECR_INTERVAL);
		},
		slowDown: function() {
			this.$store.commit(M.INCR_INTERVAL);
		},
		ff: function() {
			if(this.matchState !== 'finished') {
				this.$store.commit(M.FF);
				store.methods.setLoading(true);
			}
		}
	},
	computed : {
		... Vuex.mapState({
			matchState: (state) => state.matchState,
			resultText: (state) => state.resultText,
			intervalStep: (state) => state.intervalStep
		}),
		... Vuex.mapGetters({
			getButtonText: 'getMatchStateButtonText',
			getStateText: 'getMatchStateText'
		}),
		getResultText: function() {
			if(this.matchState !== 'finished') {
				return '';
			} else {
				return this.resultText;
			}
		}
	},
})

ScoreboardShare = Vue.component('scoreboard-share', {
	props: [],
	template: '#scoreboard-share',
	data () {
		return {
			link: ''
		}
	},
	mounted () {
		this.link = `${CONFIG.BACKEND_BASE_URL}${this.$route.path}?f=y`;
	},
	computed: {
		... Vuex.mapState({
			scorecardLink: (state) => state.scorecardLink
		})
	}
})