BHeader = Vue.component('b-header', {
	template: '#b-header',
	methods: {
		gotoHomepage: function () {
			router.push({ path: `${CONFIG.FRONTEND_BASE_URL}/` })
		}
	}
});