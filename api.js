var API = {
	getTeamList: function() {
		let promise =  new Promise((resolve, reject) => {
			let GET_TEAM_LIST = `${CONFIG.BACKEND_BASE_URL}/api/beamer/v1/team/all/summary`
			fetch(GET_TEAM_LIST)
				.then((response) => {
					// console.log(response)
					response.json().then((data) => {
						// console.log(data)
						resolve(data.data)
						// store.state.teamList = data.data
					})
				}) 
		})
		return promise;
	},
	getTeamDetails: function(teamId) {
		let promise = new Promise((resolve, reject) => {
			let GET_TEAM_DETAILS = `${CONFIG.BACKEND_BASE_URL}/api/beamer/v1/team/${teamId}/details`
			fetch(GET_TEAM_DETAILS)
				.then((response) => {
					response.json().then((data) => {
						// console.log(data);
						resolve(data.data);
					})
				})
		})
		return promise;
	},
	getStadiumList: function(id) {
		if (id === undefined) {
			id = "all"
		}
		let promise = new Promise((resolve, reject) => {
			let GET_STADIUM_DETAILS = `${CONFIG.BACKEND_BASE_URL}/api/beamer/v1/stadium/${id}/details`
			fetch(GET_STADIUM_DETAILS)
				.then((response) => {
					response.json().then((data) => {
						resolve(data.data);
					})
				})
		})
		return promise;
	},
	getMatchData: function(id) {
		let promise = new Promise((resolve, reject) => {
			let GET_MATCH_DATA = `${CONFIG.BACKEND_BASE_URL}/api/beamer/v1/match/${id}/live`;
				fetch(GET_MATCH_DATA)
					.then((response) => {
						response.json().then((data) => {
							resolve(data.data);
							// this.private.matchData = data.data;
						})
					})
		})
		return promise;
	},
	queueMatch: function(data) {
		let promise =  new Promise((resolve, reject) => {
			let QUEUE_MATCH = `${CONFIG.BACKEND_BASE_URL}/api/beamer/v1/match/simulate`;
			let fetchData = { 
				method: 'POST', 
				body: JSON.stringify(data),
				headers: {
					'Content-Type': 'application/json'
				}
			}
			fetch(QUEUE_MATCH, fetchData)
				.then((response) => {
					response.json().then((data) => {
						resolve(data.data);
					})
				});
		})
		return promise;
	}
}