var log = function(message) {
	if(store.debug) {
		console.log(message);
	}
}

var Const = {
	MATCH_TYPE: {
		ODI: "odi",
		T20: "t20",
		TEST: "test"
	}
}

var store = {
	debug: true,
	state: {
		loading: false,
		prompt: false,
		hideCloseButton: false,
		promptMessage: "",
		message: 'WALLY',
		teamList: [],
		stadiumList: [],
		matchSettings: {
			team1: {
				id: undefined,
				name: undefined,
				shortform: undefined,
				playerList: []
			},
			team2: {
				id: undefined,
				name: undefined,
				shortform: undefined,
				playerList: []
			},
			matchType: Const.MATCH_TYPE.ODI,
			stadium: {
				id: undefined,
				name: undefined,
				country: undefined
			}
		}
	},
	methods: {
		setLoading: function(bool) {
			store.state.loading = bool;
		},
		showPrompt: function(message, hideClose) {
			store.state.prompt = true;
			store.state.promptMessage = message;
			store.state.hideCloseButton = hideClose;
		},
		hidePrompt: function() {
			store.state.prompt = false;
			store.state.promptMessage = undefined;
		},
		loadTeamList: function() {
			API.getTeamList().then((data) => {
				store.state.teamList = data
			}, (err) => {
				console.log("Something went wrong!");
			})
		},
		getTeamDetails: function(teamNumber, teamId) {
			if(teamId === -1) 
				return
			API.getTeamDetails(teamId).then((data) => {
				if(teamNumber === 1) {
					store.state.matchSettings.team1.id = data.team_id;
					store.state.matchSettings.team1.name = data.team_name;
					store.state.matchSettings.team1.shortform = data.shortform;
					store.state.matchSettings.team1.playerList = data.player_list;
				} else {
					store.state.matchSettings.team2.id = data.team_id;
					store.state.matchSettings.team2.name = data.team_name;
					store.state.matchSettings.team2.shortform = data.shortform;
					store.state.matchSettings.team2.playerList = data.player_list;
				}
			})	
		},
		populateEmptyTeamData: function(teamNumber) {
			let emptyData = {
				id: undefined,
				name: undefined,
				battingName: undefined,
				fieldingName: undefined,
				country: undefined,
				playerType: undefined,
				battingStyle: undefined,
				bowlingStyle: undefined,
				playerRole: undefined,
				isDismissed: undefined,
				oversBowled: undefined,
				battingConfidence: undefined,
				bowlingConfidence: undefined 
			}
			for (let i=0; i<11; i++) {
				store.state.matchSettings.team1.playerList.push(Object.assign({}, emptyData));
				store.state.matchSettings.team2.playerList.push(Object.assign({}, emptyData));
			}
		},
		getStadiumList: function() {
			API.getStadiumList().then((data) => {
				store.state.stadiumList = data;
			}, (err) => {
				console.log("Something went wrong!")
			})
		}
	}
}


//Used only the stream page. 
//All ints have default value of 0, All string have default value of null.
function initializeStreamState () {
	var state = {
		commentary: [],
		//below object will be populated by the getMatchDetails API.
		matchData: {
			finished: null,
			stadiumId: null,
			id: null,
			matchData: {},
			team1Id: null,
			team2Id: null,
			tossDecision: null,
			tossWinner: null,
			matchType: null
		},
		stadium: {
			country: null,
			id: null,
			name: null
		},
		team1: {
			id: null,
			name: null,
			shortform: null,
			playerList: getEmptyPlayerList(),
			fow: [],
			score: {
				//runs scored by the team
				runs: null,
				//wickets lost by the team
				wickets: null,
				//overs played by the team
				overs: null,
				//number of balls processed from the inningData
				currentBall: null,
				//number of balls bowled in the current over
				overBalls: null,
				//outcome of the balls bowled in the current over
				currentOver: [],
				//chasing target if playing second inning
				target: null,
				extras: null
			}
		},
		team2: {
			id: null,
			name: null,
			shortform: null,
			playerList: getEmptyPlayerList(),
			fow: [],
			score: {
				runs: null,
				wickets: null,
				overs: null,
				currentBall: null,
				overBalls: null,
				currentOver: [],
				target: null,
				extras: 0
			}
		},
		inningsOrder: [],
		battingTeam: null,
		bowlingTeam: null,
		currentInning: null,

		currentBowler: {},
		onStrike: {},
		runnerEnd: {},
		matchState: 'running',
		resultText: '',
		intervalStep: 500,
		isFF: false,
		scorecardLink: ''
	}

	return state;
}

function getEmptyPlayerList() {
	let player = {
		id: null,
		battingName: null,
		fieldingName: null,
		name: null,
		country: null,
		playerType: null,
		playerRole: null,
		batting: {
			runs: null,
			balls: null,
			fours: null,
			sixes: null,
			dismissalText: null
		},
		bowling: {
			runs: null,
			balls: null,
			wickets: null,
			noBalls: null,
			wides: null
		}
	}

	let playerList = [];
	for(let i=0;i<11; i++) {
		playerList.push(Object.assign({}, player));
	}

	return playerList;
}


var M = {
	SET_MATCH_DATA: 'setMatchData',
	SET_TEAM_DATA: 'setTeamData',
	SET_STADIUM_DATA: 'setStadiumData',
	INIT_MATCH_STATE: 'initMatchState',
	INIT_BALL_PROCESS: 'initBallProcess',
	POST_BALL_PROCESS: 'postBallProcess',
	PROCESS_VALID_BALL: 'processValidBall',
	PROCESS_WIDE_BALL: 'processWideBall',
	PROCESS_NO_BALL: 'processNoBall',
	CONCLUDE_INNINGS: 'concludeInnings',
	TOGGLE_MATCH_STATE: 'toggleMatchState',
	INCR_INTERVAL: 'incrementInterval',
	DECR_INTERVAL: 'decrementInterval',
	FF: 'ff',
	REVERT_FF: 'revertFF',
	SET_SC_LINK: 'setScorecardLink'
}

var vxstore = new Vuex.Store({
	state: initializeStreamState(),
	mutations: {
		//After the match data is fetched from API, this method sanitises the
		//data and assigns it to various properties.
		[M.SET_MATCH_DATA] (state, data) {
			state.matchData.finished = data.finished,
			state.matchData.id =  data.id,
			state.matchData.stadiumId = data.ground_id,
			state.matchData.team1Id = data.team1_id,
			state.matchData.team2Id = data.team2_id,
			state.matchData.tossDecision = data.toss_decision,
			state.matchData.tossWinner = data.toss_winner,
			state.matchData.matchData = data.match_data,
			state.matchData.matchType = data.match_type
		},
		[M.SET_TEAM_DATA] (state, data) {
			var team = null;
			if(data.team_id === state.matchData.team1Id) {
				team = state.team1;
			} else if (data.team_id === state.matchData.team2Id) {
				team = state.team2;
			}
			
			team.id = data.team_id,
			team.name = data.team_name,
			team.shortform = data.shortform
			
			for(let i=0; i<team.playerList.length; i++) {
				let newData = {
					id: data.player_list[i].id,
					name: data.player_list[i].name,
					battingName: data.player_list[i].batting_name,
					fieldingName: data.player_list[i].fielding_name,
					country: data.player_list[i].country,
					playerRole: data.player_list[i].player_role,
					playerType: data.player_list[i].player_type,
				}
				Vue.set(team.playerList, i, Object.assign({}, team.playerList[i], newData));
			}

		},
		[M.SET_STADIUM_DATA] (state, data) {
			state.stadium.id = data.id;
			state.stadium.name = data.name;
			state.stadium.country = data.country;
		},
		[M.INIT_MATCH_STATE] (state) {
			if(state.team1.id === state.matchData.tossWinner) {
				if(state.matchData.tossDecision === "bat") {
					state.battingTeam = state.team1.id;
					state.bowlingTeam = state.team2.id;
					state.inningsOrder.push(state.team1.id, state.team2.id);
					state.currentInning = 1;
				} else {
					state.battingTeam = state.team2.id;
					state.bowlingTeam = state.team1.id;
					state.inningsOrder.push(state.team2.id, state.team1.id);
					state.currentInning = 1;
				}
			} else {
				if(state.matchData.tossDecision === "bat") {
					state.battingTeam = state.team2.id;
					state.bowlingTeam = state.team1.id;
					state.inningsOrder.push(state.team2.id, state.team1.id);
					state.currentInning = 1;
				} else {
					state.battingTeam = state.team1.id;
					state.bowlingTeam = state.team2.id;
					state.inningsOrder.push(state.team1.id, state.team2.id);
					state.currentInning = 1;
				}
			}
			state.team1.score.currentBall = 0;
			state.team1.score.overBalls = 0;
			state.team1.score.overs = 0;
			state.team1.score.runs = 0;
			state.team1.score.wickets =  0;
			state.team1.score.extras = 0;
			state.team2.score.currentBall = 0;
			state.team2.score.overBalls = 0;
			state.team2.score.overs = 0;
			state.team2.score.runs = 0;
			state.team2.score.wickets =  0;
			state.team2.score.extras = 0;
		},
		//INIT_BALL_PROCESS sets the current bowler, onstrike, and runnerend player in state.
		[M.INIT_BALL_PROCESS] (state, ballData) {
			//check if 6 valid balls have been bowled in this over, if
			//yes, reset the currentOverBalls and currentOver state.
			if(vxstore.getters.getBattingTeam.score.overBalls >= 6) {
				vxstore.getters.getBattingTeam.score.overBalls = 0;
				vxstore.getters.getBattingTeam.score.currentOver = [];
				vxstore.getters.getBattingTeam.score.overs += 1;
				state.commentary.push(vxstore.getters.getCommentaryText(null, true));
			}
			
			let defaultBatsman = {
				balls: 0,
				dismissalText: 'not out',
				fours: 0,
				sixes: 0,
				runs: 0
			}
			let defaultBowler = {
				balls: 0,
				noBalls: 0,
				runs: 0,
				wickets: 0,
				wides: 0
			}

			//get the currentbowler, onstrike and runnerend players from balldata
			state.currentBowler = vxstore.getters
					.getPlayerById(vxstore.getters.getBowlingTeam, ballData.bowler);
			state.onStrike = vxstore.getters
					.getPlayerById(vxstore.getters.getBattingTeam, ballData.on_strike);
			state.runnerEnd = vxstore.getters
					.getPlayerById(vxstore.getters.getBattingTeam, ballData.runner_end);
			
			//if this is the first ball by the bowler, set it's initial state of
			//bowling stats to 0.
			if(state.currentBowler.bowling.runs === null) {
				state.currentBowler.bowling = Object.assign({}, defaultBowler);
			}

			//if this it the first ball faced by the onstrike batsman, set it's
			//initial state of batting stats to 0
			if(state.onStrike.batting.runs === null) {
				state.onStrike.batting = Object.assign({}, defaultBatsman);
			}

			//if this it the first ball faced by the runner batsman, set it's
			//initial state of batting stats to 0
			if(state.runnerEnd.batting.runs === null) {
				state.runnerEnd.batting = Object.assign({}, defaultBatsman);
			}
		},
		[M.POST_BALL_PROCESS] (state, payload) {
			vxstore.getters.getBattingTeam.score.currentBall += 1;
			vxstore.getters.getBattingTeam.score.currentOver.push({ batOc: payload.ballData.batting_outcome, ballOc: payload.ballData.bowling_outcome});
			state.commentary.push(vxstore.getters.getCommentaryText(payload.ballData, false));
		},
		[M.PROCESS_VALID_BALL] (state, payload) {
			let battingTeam = vxstore.getters.getBattingTeam;
			let battingOutcome = 0;
			if(!payload.isOut) {
				battingOutcome = Number.parseInt(payload.ballData.batting_outcome);
			}

			if(!payload.isOut && payload.ballData.is_wicket == 0) {
				//If the ball is valid and runs are scored.
				battingTeam.score.runs += battingOutcome;
				battingTeam.score.overBalls += 1;
				
				//update the onstrike batsman score
				state.onStrike.batting.runs += battingOutcome;
				state.onStrike.batting.balls += 1;

				if(battingOutcome === 4) {
					state.onStrike.batting.fours += 1;
				}

				if(battingOutcome === 6) {
					state.onStrike.batting.sixes += 1;
				}
				
				//update the bowler stats
				state.currentBowler.bowling.runs += battingOutcome;
				state.currentBowler.bowling.balls += 1;
			} else if (payload.isOut && payload.ballData.is_wicket == 1) {
				//If the ball is valid and a wicket was taken.
				battingTeam.score.wickets += 1;
				battingTeam.score.overBalls += 1;
				battingTeam.fow.push(`${battingTeam.score.runs}-${battingTeam.score.wickets}`)

				//update the onstrike batsman score
				state.onStrike.batting.balls += 1;

				//update the bowler stats
				if(payload.ballData.batting_outcome !== "runout") {
					state.currentBowler.bowling.wickets += 1;
				}
				state.currentBowler.bowling.balls += 1;

				//update the stats of dismissed batsman (it's not necessary that the onstrike
				//batsman was dismissed)
				let dismissed = vxstore.getters.getPlayerById(vxstore.getters.getBattingTeam, payload.ballData.dismissed_batsman);
				dismissed.batting.dismissalText = vxstore.getters.getDismissalText(payload.ballData);

			} else {
				//The ball was valid and a wicket fell, but the ball was a free hit
				battingTeam.score.overBalls += 1;
				state.onStrike.batting.balls += 1;
				state.currentBowler.bowling.balls += 1;
			}
		},
		[M.PROCESS_WIDE_BALL] (state, payload) {
			let battingTeam = vxstore.getters.getBattingTeam;
			let battingOutcome = 0;
			if(!payload.isOut) {
				battingOutcome = Number.parseInt(payload.ballData.batting_outcome);
			}

			if(!payload.isOut && payload.ballData.is_wicket == 0) {
				//If the ball bowled was wide and no wicket fell.
				battingTeam.score.runs += 1 + battingOutcome;
				battingTeam.score.extras += 1 + battingOutcome;

				//update the bowler stats
				state.currentBowler.bowling.runs += 1 + battingOutcome;
				state.currentBowler.bowling.wides += 1 + battingOutcome;
			} else if (payload.isOut && payload.ballData.is_wicket == 1) {
				//wide ball but wicket was taken
				battingTeam.score.wickets += 1
				battingTeam.score.runs += 1;
				battingTeam.score.extras += 1;

				//update the bowler stats
				state.currentBowler.bowling.runs += 1;
				state.currentBowler.bowling.wides += 1;	
				if(payload.ballData.batting_outcome !== "runout") {
					state.currentBowler.bowling.wickets += 1;
				}

				let dismissed = vxstore.getters.getPlayerById(vxstore.getters.getBattingTeam, payload.ballData.dismissed_batsman);
				dismissed.batting.dismissalText = vxstore.getters.getDismissalText(payload.ballData);
			} else {
				//The ball was wide and a freehit
				battingTeam.score.runs += 1;
				battingTeam.score.extras += 1;
				state.currentBowler.bowling.runs += 1;
				state.currentBowler.bowling.wides += 1;
			}
		},
		[M.PROCESS_NO_BALL] (state, payload) {
			let battingTeam = vxstore.getters.getBattingTeam;
			let battingOutcome = 0;
			if(!payload.isOut) {
				battingOutcome = Number.parseInt(payload.ballData.batting_outcome);
			}

			if(!payload.isOut && payload.ballData.is_wicket == 0) {
				//If the ball bowled was no ball and no wicket fell.
				battingTeam.score.runs += 1 + battingOutcome;
				battingTeam.score.extras += 1;

				//update the bowler stats
				state.currentBowler.bowling.runs += 1 + battingOutcome;
				state.currentBowler.bowling.noBalls += 1;
			} else if (payload.isOut && payload.ballData.is_wicket == 1) {
				//no ball but wicket was taken
				battingTeam.score.wickets += 1
				battingTeam.score.runs += 1;
				battingTeam.score.extras += 1;

				//update the bowler stats
				state.currentBowler.bowling.runs += 1;
				state.currentBowler.bowling.noBalls += 1;
				if(payload.ballData.batting_outcome !== "runout") {
					state.currentBowler.bowling.wickets += 1;
				}

				let dismissed = vxstore.getters.getPlayerById(vxstore.getters.getBattingTeam, payload.ballData.dismissed_batsman);
				dismissed.batting.dismissalText = vxstore.getters.getDismissalText(payload.ballData);	
			} else {
				//The ball was noball and a freehit
				battingTeam.score.runs += 1;
				battingTeam.score.extras += 1;
				state.currentBowler.bowling.runs += 1;
				state.currentBowler.bowling.noBalls += 1;
			}
		},
		[M.CONCLUDE_INNINGS] (state, payload) {
			if(state.currentInning === 1) {
				state.currentInning = 2;
				let temp = state.battingTeam;
				state.battingTeam = state.bowlingTeam;
				state.bowlingTeam = temp;

				vxstore.getters.getBattingTeam.score.target = vxstore.getters.getBowlingTeam.score.runs;
			} else if (state.currentInning == 2) {
				state.matchState = 'finished';
				let secondInn = vxstore.getters.getBattingTeam;
				let firstInn = vxstore.getters.getBowlingTeam;
				if(secondInn.score.runs > firstInn.score.runs) {
					state.resultText = `${secondInn.name} won by ${10 - secondInn.score.wickets} wicket(s).`;
				} else if (secondInn.score.runs < firstInn.score.runs) {
					state.resultText = `${firstInn.name} won by ${secondInn.score.target - secondInn.score.runs + 1} run(s).`;
				} else {
					state.resultText = `Match drawn.`;
				}
			}
		},
		[M.TOGGLE_MATCH_STATE] (state) {
			if(this.state.matchState === 'running') {
				this.state.matchState = 'paused';
			} else if (this.state.matchState === 'paused') {
				this.state.matchState = 'running';
			}	
		},
		[M.INCR_INTERVAL] (state) {
			state.intervalStep += 200;
		},
		[M.DECR_INTERVAL] (state) {
			if(state.intervalStep - 200 > 0) {
				state.intervalStep -= 200;
			}
		},
		[M.FF] (state) {
			state.intervalStep = 0;
			state.isFF = true;	
		},
		[M.REVERT_FF] (state) {
			state.intervalStep = 500;
			state.isFF = false;
		},
		[M.SET_SC_LINK] (state, payload) {
			state.scorecardLink = payload.link;
		}
	},
	getters: {
		getTeamById: (state) => (id) => {
			if(state.team1.id === id) {
				return state.team1;
			} else if (state.team2.id === id) {
				return state.team2;
			}
			// console.log(`Team not found: ${id}`);
			return null;
		},
		getBattingTeam: (state) => {
			return vxstore.getters.getTeamById(state.battingTeam);
		},
		getBowlingTeam: (state) => {
			return vxstore.getters.getTeamById(state.bowlingTeam);
		},
		getPlayerById: (state) => (team, playerId) => {
			for(let i=0; i<team.playerList.length; i++) {
				if(team.playerList[i].id == playerId) {
					return team.playerList[i];
				}
			}
			console.error(`Player not found: ${playerId}`)
			return null;
		},
		//This method shouldn't be in getters as it's not fetching any data 
		//from the store, but there's no better place to put it. 
		getDismissalText: (state) => (ballData) => {
			let fielder = ballData.fielder !== null ? vxstore.getters.getPlayerById(vxstore.getters.getBowlingTeam, ballData.fielder) : null;
			let bowler = state.currentBowler;
			let dismissed = ballData.dismissed_batsman !== null ? vxstore.getters.getPlayerById(vxstore.getters.getBattingTeam, ballData.dismissed_batsman) : null;
			let text = '';
			switch (ballData.batting_outcome) {
				case CONSTS.DISMISSAL.BOWLED:
					return `b ${bowler.fieldingName}`
				case CONSTS.DISMISSAL.CAUGHT:
					return `c ${fielder.fieldingName} b ${bowler.fieldingName}`
				case CONSTS.DISMISSAL.RUNOUT:
					return `runout ${fielder.fieldingName}`
				case CONSTS.DISMISSAL.STUMPED:
					return `st ${fielder.fieldingName} b ${bowler.fieldingName}`
				case CONSTS.DISMISSAL.LBW:
					return `lbw b ${bowler.fieldingName}`
			}
			console.err("Fatal. Unhandled batting outcome.");
		},
		getCommentaryText: (state) => (ballData, overEnd) => {	
			let battingTeam = vxstore.getters.getBattingTeam;
			let onStrike = state.onStrike;
			let runnerEnd = state.runnerEnd;
			if(!overEnd) {
				let currentBowler = state.currentBowler;
				let battingOutcome = ballData.batting_outcome;
				let bowlingOutcome = ballData.bowling_outcome;
				let overText = `${battingTeam.score.overs}.${battingTeam.score.overBalls}`;

				if (bowlingOutcome === CONSTS.BOWLING_OUTCOMES.NO_BALL) {
					return ({
						type: 'ball',
						outcome: `noball`,
						data: `(${overText}) ${currentBowler.fieldingName} to ${onStrike.battingName}. (NO BALL) ${battingOutcome}`
					});
				} else if (bowlingOutcome === CONSTS.BOWLING_OUTCOMES.WIDE) {
					return ({
						type: 'ball',
						outcome: `wide`,
						data: `(${overText}) ${currentBowler.fieldingName} to ${onStrike.battingName}. (WIDE) ${battingOutcome}`
					});
				} else {
					let outcome = ''
					if( battingOutcome === CONSTS.DISMISSAL.CAUGHT ||
						battingOutcome === CONSTS.DISMISSAL.RUNOUT ||
						battingOutcome === CONSTS.DISMISSAL.BOWLED ||
						battingOutcome === CONSTS.DISMISSAL.STUMPED ||
						battingOutcome === CONSTS.DISMISSAL.LBW ||
						battingOutcome === CONSTS.DISMISSAL.HIT_WICKET) {
							outcome = 'wicket';
						} else if (battingOutcome === '4' || battingOutcome === '6') {
							outcome = 'boundary';
						}
					return ({
						type: 'ball',
						outcome: outcome,
						data: `(${overText}) ${currentBowler.fieldingName} to ${onStrike.battingName}. ${battingOutcome}`
					});
				}
			} else {
				
				return ({
					type: 'overEnd',
					outcome: 'overEnd',
					data: `${battingTeam.shortform}: ${battingTeam.score.runs} - ${battingTeam.score.wickets} | 
							${onStrike.battingName}: ${onStrike.batting.runs}(${onStrike.batting.balls}), ${runnerEnd.battingName}: ${runnerEnd.batting.runs}(${runnerEnd.batting.balls})`
				})
			}
			
		},
		getMatchStateButtonText: (state) => {
			if(state.matchState === 'running') {
				return ' ⏸ ';
			} else if (state.matchState === 'paused') {
				return ' ▶ ';
			} else {
				return 'Finished';
			}
		},
		getMatchStateText: (state) => {
			if(state.matchState === 'running') {
				return 'Match is in progress.';
			} else if (state.matchState === 'paused') {
				return 'Match is paused.';
			} else {
				return 'Match is finished.';
			}
		}
	}
})
