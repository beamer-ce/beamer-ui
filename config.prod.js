var CONFIG = {
	BACKEND_BASE_URL: 'https://nbn.sh',
	FRONTEND_BASE_URL: '/p/beamer'
}

var CONSTS = {
	DISMISSAL: {
		CAUGHT: "caught",
		RUNOUT: "runout",
		BOWLED: "bowled",
		STUMPED: "stumped",
		LBW: "leg before wicket",
		HIT_WICKET: "hit wicket"
	},
	BOWLING_OUTCOMES: {
		GREAT: "great",
		GOOD: "good",
		OK: "ok",
		BAD: "bad",
		WIDE: "wide",
		NO_BALL: "no_ball"
	},
	MATCH_TYPE: {
		ODI: "odi",
		T20: "t20",
		TEST: "test",
		MAX_OVERS: {
			"odi": 50,
			"t20": 20,
			"test": -1
		},
		MAX_OVERS_PER_BOWLER: {
			"odi": 10,
			"t20": 4
		}
	},
	MSG: {
		PROCESSING_MESSAGE: 'Match simulation is in progress. Please wait for a few seconds and refresh the page to check again. Page will refresh automatically in 5 seconds.'
	}
}

